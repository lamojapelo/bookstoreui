import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TitleComponent } from './components/title/title.component';
import { DefaultLayoutComponent } from './components/default-layout/default-layout.component';
import { WordWrapPipe } from './pipes/word-wrap.pipe';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [TitleComponent, DefaultLayoutComponent, WordWrapPipe],
  imports: [
    CommonModule,MatIconModule, MatButtonModule, MatTooltipModule, MatMenuModule, RouterModule
  ],
  exports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
    MatTooltipModule,
    DefaultLayoutComponent,
    TitleComponent,
    WordWrapPipe
  ]
})
export class SharedModule { }
