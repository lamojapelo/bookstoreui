import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroupDirective, ValidationErrors, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/services/authentication/authentication.service';
import { HeaderService } from 'src/app/core/services/header.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    userName: ['', [Validators.required, Validators.minLength(6)]],
    password: ['', [Validators.required, Validators.minLength(8)]],
    confirmedPassword: ['', [Validators.required]]
  }, { validators: this.matchPasswords });

  @ViewChild(FormGroupDirective) sufDirective: FormGroupDirective | undefined;

  constructor(
    private account: AuthenticationService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router: Router,
    private header: HeaderService
  ) { }

  ngOnInit() {
    this.header.setHeaderButtonsVisibility(false);
  }

  matchPasswords(signupGroup: AbstractControl): ValidationErrors | null {
    const password = signupGroup.get('password')?.value;
    const confirmedPassword = signupGroup.get('confirmedPassword')?.value;

    return password == confirmedPassword ? null : { differentPasswords: true };
  }

  get password() { return this.signupForm.get('password'); }

  get confirmedPassword() { return this.signupForm.get('confirmedPassword'); }

  get userName() { return this.signupForm.get('userName'); }

  signup() {
    const customer = this.signupForm.value;

    this.account.register(
      customer.email,
      customer.password,
      customer.confirmedPassword,
      customer.firstName,
      customer.lastName,
      customer.userName
    ).subscribe(
      (results: any) => {
        this.signupForm.reset();
        this.sufDirective?.resetForm();

        this.snackBar.open('Account successfully created. Please check your email for verification.', 'Close', { duration: 5000 });
        //this.snackBar.open(results.body.message, 'Close', { duration: 10000 });
        window.prompt("Copy link to clipboard: Ctrl+C, Enter",results.body.message)

        setTimeout(() => this.router.navigateByUrl('/'), 6000);
      },
      err => this.snackBar.open('There was a problem creating your account.', 'Close', { duration: 5000 })
    );
  }

}
