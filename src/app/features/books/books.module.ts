import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '@angular/cdk/layout';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';

import { SharedModule } from 'src/app/shared/shared.module';
import { BookListComponent } from './pages/book-list/book-list.component';
import { BookComponent } from './pages/book/book.component';


@NgModule({
  declarations: [BookListComponent, BookComponent],
  imports: [
    RouterModule.forChild([
      { path: 'book/:id', component: BookComponent },
      { path: '', component: BookListComponent }
    ]),
    LayoutModule,
    MatCardModule,
    MatGridListModule,
    MatPaginatorModule,
    SharedModule
  ]
})
export class BooksModule { }
