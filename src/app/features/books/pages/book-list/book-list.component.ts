import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/core/services/header.service';
import { PagedResponse } from 'src/app/data/common/models/paged-response';
import { Book } from 'src/app/data/models/book';
import { BookService } from 'src/app/data/services/book.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  cols = 4;

  length = 0;
  pageIndex = 0;
  pageSize = 20;
  pageSizeOptions: number[] = [5, 10, 20];
  books: Book[] = [];

  pageEvent!: PageEvent | void;


  constructor(private breakpointObserver: BreakpointObserver,
    private bookService: BookService,
    private router: Router,
    private header: HeaderService) { }

  ngOnInit(): void {
    this.getBooks(1, 20);

    this.header.setHeaderButtonsVisibility(true);

    this.breakpointObserver.observe([
      Breakpoints.Handset,
      Breakpoints.Tablet,
      Breakpoints.Web
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints['(max-width: 599.98px) and (orientation: portrait)'] || result.breakpoints['(max-width: 599.98px) and (orientation: landscape)']) {
          this.cols = 1;
        }
        else if (result.breakpoints['(min-width: 1280px) and (orientation: portrait)'] || result.breakpoints['(min-width: 1280px) and (orientation: landscape)']) {
          this.cols = 4;
        } else {
          this.cols = 3;
        }
      }
    });
  }

  private getBooks(page: number, pageSize: number) {
    this.bookService.getBooks(page, pageSize)
      .subscribe(
        books => {
          this.books = books.data;
          this.length = books.recordCount;
        },
        err => this.router.navigateByUrl('/error')
      );
  }

  getNextPage(event: PageEvent) {
    this.getBooks(event.pageIndex + 1, event.pageSize);
  }

  trackBooks(index: number, item: Book) {
    return `${item.id}-${index}`;
  }

}
