import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandlerService } from 'src/app/shared/services/http-error-handler.service';
import { environment } from 'src/environments/environment';
import { PagedResponse } from '../common/models/paged-response';
import { GetResponse } from '../common/models/response';
import { Book } from '../models/book';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  private url: string = `${environment.baseUrl}/v1/book`;

  constructor(private http: HttpClient, private eh: HttpErrorHandlerService) { }

  getBook(id: number): Observable<GetResponse<Book>> {
    return this.http.get<GetResponse<Book>>(`${this.url}/${id}`)
      .pipe(catchError(this.eh.handleError));
  }

  getBooks(page: number, pageSize: number): Observable<PagedResponse<Book>> {
    return this.http.get<PagedResponse<Book>>(
      this.url,
      {
        params: {
          'pageNumber': page.toString(),
          'pageSize': pageSize.toString()
        }
      })
      .pipe(catchError(this.eh.handleError));
  }

}
