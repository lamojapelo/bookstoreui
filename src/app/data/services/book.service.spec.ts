import { inject, TestBed } from '@angular/core/testing';
import { PagedResponse } from '../common/models/paged-response';
import { Book } from '../models/book';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { BookService } from './book.service';
import { GetResponse } from '../common/models/response';

describe('BookService', () => {
  let service: BookService;

  beforeEach((done) => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(BookService);
    window.jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
        setTimeout(function () {
            console.log('inside timeout');
            done();
        }, 500);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getBook should return book object from observable',
    (done: DoneFn) => {
      service.getBook(2).subscribe(value => {
        expect(value).toBe(response);
        done();
    });
  });

});

export const pagedResponse: PagedResponse<Book> = 
{
  "pageNumber": 1,
  "pageSize": 10,
  "recordCount": 1,
  "succeeded": true,
  "message": null,
  "errors": null,
  "data": [
    {
      "id": 2,
      "name": "Name",
      "barcode": "12345-ABC",
      "description": "Book description",
      "purchasePrice": 100
    }
  ]
}

export const response: GetResponse<Book> = 
{
  "succeeded": true,
  "message": null,
  "errors": null,
  "data": {
    "id": 2,
    "name": "Name",
    "barcode": "12345-ABC",
    "description": "Book description",
    "purchasePrice": 100
  }
}
