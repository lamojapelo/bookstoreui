export interface Book {
    id: number;
    name: string;
    barcode: string;
    description:string;
    purchasePrice: number;
    imageUrl: string;
}
