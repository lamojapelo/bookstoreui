export interface PagedResponse<T> {
    pageNumber: number;
    pageSize: number;
    recordCount: number;
    data:T[];
    message: string;
    succeeded: boolean;
    errors: string;
}
