export interface GetResponse<T> {
    succeeded: boolean;
    message: string;
    errors: string;
    data:T;
}
