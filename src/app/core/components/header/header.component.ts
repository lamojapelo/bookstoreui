import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { HeaderService } from '../../services/header.service';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  cartAmount: number = 0;

  isLoggedIn: boolean = false;
  showButtons: boolean = true;

  constructor(
    private session: SessionService,
    private snackBar: MatSnackBar,
    //private cart: CartService,
    private header: HeaderService,
    private auth: AuthenticationService
  ) { }

  ngOnInit() {
    this.session.isCustomerLoggedIn()
      .subscribe(
        () => {
          this.isLoggedIn = false;
          this.session.setLoggedInStatus(false); //Todo 
        }
      );

    this.session.loggedInStatus$.subscribe(status => this.isLoggedIn = status);

    this.header.showHeaderButtons$.subscribe(visible => this.showButtons = visible);

    //this.cart.cartValue.subscribe(cart => this.cartAmount = cart.itemCount);
  }

}
