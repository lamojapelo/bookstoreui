import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpErrorHandlerService } from 'src/app/shared/services/http-error-handler.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  private url: string = `${environment.baseUrl}/account`;

  constructor(private http: HttpClient, private eh: HttpErrorHandlerService) { }


  login(email: string, password: string): Observable<object> {
    return this.http.post<object>(
      `${this.url}/authenticate`,
      { email: email, password: password},
      { headers: { 'Content-Type': 'application/json' }, observe: 'response' })
      .pipe(catchError(this.eh.handleError));
  }

  //todo create interface for this
  register(email: string, password: string, confirmedPassword: string,
     firstName: string, lastName: string, userName: string): Observable<object> {
      return this.http.post<object>(
        `${this.url}/register`,
        { firstName: firstName, lastName: lastName, email: email, userName:userName, password: password, confirmPassword:confirmedPassword },
        { headers: { 'Content-Type': 'application/json' }, observe: 'response' })
        .pipe(catchError(this.eh.handleError));
  }
}
